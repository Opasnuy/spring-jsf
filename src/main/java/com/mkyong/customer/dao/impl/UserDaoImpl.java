package com.mkyong.customer.dao.impl;

import java.util.List;

import com.mkyong.customer.dao.UserDao;
import com.mkyong.customer.model.User;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class UserDaoImpl extends
        HibernateDaoSupport implements UserDao {

    public void addUser(User user) {

        getHibernateTemplate().save(user);

    }

    public List<User> findAllUsers() {

        return getHibernateTemplate().find("from User");

    }
}