package com.mkyong.customer.dao;
 
import com.mkyong.customer.model.User;

import java.util.List;

public interface UserDao {
 
	void addUser(User user);
 
	List<User> findAllUsers();
 
}