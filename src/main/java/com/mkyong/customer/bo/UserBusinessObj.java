package com.mkyong.customer.bo;
 
import com.mkyong.customer.model.User;

import java.util.List;
 
public interface UserBusinessObj {
 
	void addUser(User user);
 
	List<User> findAllUser();
 
}