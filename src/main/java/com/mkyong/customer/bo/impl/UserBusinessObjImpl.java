package com.mkyong.customer.bo.impl;
 
import com.mkyong.customer.bo.UserBusinessObj;
import com.mkyong.customer.dao.UserDao;
import com.mkyong.customer.model.User;

import java.util.List;
 
public class UserBusinessObjImpl implements UserBusinessObj {
 
	UserDao userDao;
 
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
 
	public void addUser(User user){
 
		userDao.addUser(user);
 
	}
 
	public List<User> findAllUser(){
 
		return userDao.findAllUsers();
	}
}