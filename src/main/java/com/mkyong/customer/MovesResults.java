package com.mkyong.customer;


public enum MovesResults {
    UP {
        @Override
        public String getResult() {
            return "знание приобрел";
        }
    },
    LEFT {
        @Override
        public String getResult() {
            return "жизнь потерял";
        }
    },
    RIGHT {
        @Override
        public String getResult() {
            return "жизнь познал";
        }
    };

    public abstract String getResult();
}
