package com.mkyong;

import com.mkyong.customer.MovesResults;
import com.mkyong.customer.bo.UserBusinessObj;
import com.mkyong.customer.model.User;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserBean implements Serializable{

	UserBusinessObj userBo;
	
	public String name;
	public String moves;

    private ArrayList<String> movesHistory = new ArrayList<String>();

    public void setUserBo(Object userBo) {
        this.userBo = (UserBusinessObj) userBo;
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMoves() {
		return moves;
	}

	public void setMoves(String move) {
		this.moves = move;
	}
 
	//get all user data from database
	public List<User> getUserList(){
		return userBo.findAllUser();
	}
	
	//add a new user data into database
	public String addUser(){
		
		User user = new User();
        user.setName(getName());
        user.setMoves(movesHistory.toString());
		
		userBo.addUser(user);
		
		clearForm();
		
		return "";
	}

    public String checkUser(){
       ArrayList<User> users = (ArrayList<User>)  userBo.findAllUser();
       for (User user : users){
           if(user.getName().equals(getName())) {
               setInfoMessage("этот герой уже избрал свой путь");
               return  "main";
           }
       }
        return "choise";
    }

	private void clearForm(){
		setName("");
		setMoves("");
	}


    protected void setInfoMessage(String summary) {
        FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null));

    }

    public String doMove(){
        if(getMoves().equals("LEFT")){
            movesHistory.add(getMoves());
            addUser();
            movesHistory.clear();
            return "history";
        } else {
            resultOfMove(getMoves());
            movesHistory.add(getMoves());
        }
        return "result";
    }

    private String resultOfMove(String move){
        setInfoMessage(MovesResults.valueOf(move).getResult());
        return "result";
    }


}
